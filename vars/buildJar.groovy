#!/user/bin/env groovy

def call() {
    echo "building the application from Branch $BRANCH_NAME"
    sh 'mvn package'
}